package com.project2.trendyol.controller;

import com.project2.trendyol.entity.Products;
import com.project2.trendyol.service.ProductService;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@AllArgsConstructor
public class ProductController {

    private final ProductService service;

    @PostMapping("/product/save")
    public Products save(Products product){
        var response = service.save(product);
        log.info("Product was added on list");
        return response;
    }

    @GetMapping("/products/listAll")
    public List<Products> getAllProducts(){
        var response = service.getAllProducts();
        log.info("All products are shown");
        return response;
    }

    @PutMapping("/product/update/{id}/{newNumber}")
    public int updateById(@PathVariable Long id, @PathVariable int newNumber){
        var response = service.updateProduct(id, newNumber);
        log.info("Number of products are updated");
        return response;
    }

    @DeleteMapping("/product/delete/{id}")
    public void deleteById(@PathVariable Long id){
        service.deleteById(id);
        log.info("{} ID product was deleted", id);
    }

    @GetMapping("/findById/{id}")
    public Products findById(@PathVariable Long id){
        var response = service.findById(id);
        log.info("Product ID is {}", id);
        return response;
    }


}
