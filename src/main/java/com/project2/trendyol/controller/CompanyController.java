package com.project2.trendyol.controller;

import com.project2.trendyol.entity.Companies;
import com.project2.trendyol.service.CompanyService;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
public class CompanyController {

    private final CompanyService service;

    @PostMapping("/company/save")
    public Companies save(Companies companies){
        var response = service.save(companies);
        log.info("Company name was added");
        return response;
    }
}
