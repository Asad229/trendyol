package com.project2.trendyol.controller;

import com.project2.trendyol.entity.Customers;
import com.project2.trendyol.service.CustomerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
public class CustomerController {

    private final CustomerService service;

    @PostMapping("/registerCustomer")
    public Customers register(@RequestBody Customers customer){
    var response = service.register(customer);
    log.info("Customer was registered");
    return response;
    }

}
