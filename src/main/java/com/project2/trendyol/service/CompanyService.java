package com.project2.trendyol.service;

import com.project2.trendyol.entity.Companies;
import com.project2.trendyol.repository.CompanyRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class CompanyService {

    private final CompanyRepository repository;

    public Companies save(Companies companies){
        var response = repository.save(companies);
        log.info("Company details were added");
        return response;
    }
}
