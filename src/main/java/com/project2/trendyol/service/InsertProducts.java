package com.project2.trendyol.service;

import com.project2.trendyol.entity.Products;
import com.project2.trendyol.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class InsertProducts {

    private final ProductRepository repository;

    public void createData(){

        Products products = new Products();
        products.setId(1L);
        products.setName("Laptop");
        products.setNumberOfProduct(5);
        repository.save(products);

        Products products1 = new Products();
        products1.setId(2L);
        products1.setName("Computer");
        products1.setNumberOfProduct(3);
        repository.save(products1);
    }

}
