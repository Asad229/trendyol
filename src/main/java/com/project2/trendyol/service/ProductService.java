package com.project2.trendyol.service;

import com.project2.trendyol.entity.Products;
import com.project2.trendyol.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Slf4j
@Service
@RequiredArgsConstructor
public class ProductService {

    private final ProductRepository repository;

    public Products save(Products products){
        var response = repository.save(products);
        log.info("Product was added");
        return response;
    }

    @Transactional
    public int updateProduct(Long id, int num){
        var response = repository.updateProductById(id, num);
        log.trace("Product is updating");
        return response;
    }

    public List<Products> getAllProducts(){
        var response = repository.findAll();
//        var list = StreamSupport.stream(response.spliterator(), false).collect(Collectors.toList());
        log.info("Get all products");
        response.stream().forEach(i -> System.out.println(i));
        return response;
    }

    public void deleteById(Long id){

        repository.deleteById(id);
    }

    public Products findById(Long id){
        var response = repository.findById(id);
        return response.get();
    }

    public Products buy(Products products){
        return null;
    }

}
