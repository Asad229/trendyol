package com.project2.trendyol.service;

import com.project2.trendyol.entity.Customers;
import com.project2.trendyol.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class CustomerService {

    private final CustomerRepository repository;

    public Customers register(Customers customer){
        var response = repository.save(customer);
        log.info("Customer was registered successfully");
        return response;
    }

}
