package com.project2.trendyol.repository;

import com.project2.trendyol.entity.Products;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Products, Long> {

    @Modifying
    @Query(nativeQuery = true, value = "update products set numberOfProduct=:newNumberOfProduct where id=:id")
    int updateProductById(Long id, int newNumberOfProduct);
}
