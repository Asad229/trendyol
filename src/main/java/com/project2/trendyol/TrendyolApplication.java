package com.project2.trendyol;

import com.project2.trendyol.service.InsertProducts;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class TrendyolApplication implements CommandLineRunner {

	private final InsertProducts insertProducts;

	public static void main(String[] args) {
		SpringApplication.run(TrendyolApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		System.out.println("Info was saved");
		System.out.println("same changes");
		insertProducts.createData();

	}
}
